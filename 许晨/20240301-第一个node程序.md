<<<<<<< HEAD
第一行总是写上'use strict';是因为我们总是以严格模式运行JavaScript代码，避免各种潜在陷阱。

然后，选择一个目录，例如C:\Workspace，把文件保存为hello.js，就可以打开命令行窗口，把当前目录切换到hello.js所在目录，然后输入以下命令运行这个程序了：

C:\Workspace>node hello.js
Hello, world.
也可以保存为别的名字，比如first.js，但是必须要以.js结尾。此外，文件名只能是英文字母、数字和下划线的组合。

如果当前目录下没有hello.js这个文件，运行node hello.js就会报错：

C:\Workspace>node hello.js
module.js:338
    throw err;
          ^
Error: Cannot find module 'C:\Workspace\hello.js'
    at Function.Module._resolveFilename
    at Function.Module._load
    at Function.Module.runMain
    at startup
    at node.js
报错的意思就是，没有找到hello.js这个文件，因为文件不存在。这个时候，就要检查一下当前目录下是否有这个文件了。

## 命令行模式和Node交互模式

请注意区分命令行模式和Node交互模式。

看到类似C:>是在Windows提供的命令行模式：

run-node-hello
在命令行模式下，可以执行node进入Node交互式环境，也可以执行node hello.js运行一个.js文件。

看到>是在Node交互式环境下：

node-interactive-env
在Node交互式环境下，我们可以输入JavaScript代码并立刻执行。

此外，在命令行模式运行.js文件和在Node交互式环境下直接运行JavaScript代码有所不同。Node交互式环境会把每一行JavaScript代码的结果自动打印出来，但是，直接运行JavaScript文件却不会。

使用严格模式

如果在JavaScript文件开头写上'use strict';，那么Node在执行该JavaScript时将使用严格模式。但是，在服务器环境下，如果有很多JavaScript文件，每个文件都写上'use strict';很麻烦。我们可以给Nodejs传递一个参数，让Node直接为所有js文件开启严格模式：

node --use_strict calc.js
后续代码，如无特殊说明，我们都会直接给Node传递--use_strict参数来开启严格模式。

## 小结

- 用文本编辑器写JavaScript程序，然后保存为后缀为.js的文件，就可以用node直接运行这个程序了。

- Node的交互模式和直接运行.js文件有什么区别呢？

- 直接输入node进入交互模式，相当于启动了Node解释器，但是等待你一行一行地输入源代码，每输入一行就执行一行。

- 直接运行node hello.js文件相当于启动了Node解释器，然后一次性把hello.js文件的源代码给执行了，你是没有机会以交互的方式输入源代码的。

=======
第一行总是写上'use strict';是因为我们总是以严格模式运行JavaScript代码，避免各种潜在陷阱。

然后，选择一个目录，例如C:\Workspace，把文件保存为hello.js，就可以打开命令行窗口，把当前目录切换到hello.js所在目录，然后输入以下命令运行这个程序了：

C:\Workspace>node hello.js
Hello, world.
也可以保存为别的名字，比如first.js，但是必须要以.js结尾。此外，文件名只能是英文字母、数字和下划线的组合。

如果当前目录下没有hello.js这个文件，运行node hello.js就会报错：

C:\Workspace>node hello.js
module.js:338
    throw err;
          ^
Error: Cannot find module 'C:\Workspace\hello.js'
    at Function.Module._resolveFilename
    at Function.Module._load
    at Function.Module.runMain
    at startup
    at node.js
报错的意思就是，没有找到hello.js这个文件，因为文件不存在。这个时候，就要检查一下当前目录下是否有这个文件了。

## 命令行模式和Node交互模式

请注意区分命令行模式和Node交互模式。

看到类似C:>是在Windows提供的命令行模式：

run-node-hello
在命令行模式下，可以执行node进入Node交互式环境，也可以执行node hello.js运行一个.js文件。

看到>是在Node交互式环境下：

node-interactive-env
在Node交互式环境下，我们可以输入JavaScript代码并立刻执行。

此外，在命令行模式运行.js文件和在Node交互式环境下直接运行JavaScript代码有所不同。Node交互式环境会把每一行JavaScript代码的结果自动打印出来，但是，直接运行JavaScript文件却不会。

使用严格模式

如果在JavaScript文件开头写上'use strict';，那么Node在执行该JavaScript时将使用严格模式。但是，在服务器环境下，如果有很多JavaScript文件，每个文件都写上'use strict';很麻烦。我们可以给Nodejs传递一个参数，让Node直接为所有js文件开启严格模式：

node --use_strict calc.js
后续代码，如无特殊说明，我们都会直接给Node传递--use_strict参数来开启严格模式。

## 小结

- 用文本编辑器写JavaScript程序，然后保存为后缀为.js的文件，就可以用node直接运行这个程序了。

- Node的交互模式和直接运行.js文件有什么区别呢？

- 直接输入node进入交互模式，相当于启动了Node解释器，但是等待你一行一行地输入源代码，每输入一行就执行一行。

- 直接运行node hello.js文件相当于启动了Node解释器，然后一次性把hello.js文件的源代码给执行了，你是没有机会以交互的方式输入源代码的。

>>>>>>> 9162b40ce73619fd3871fa7d93fb880d7dcd598d
- 在编写JavaScript代码的时候，完全可以一边在文本编辑器里写代码，一边开一个Node交互式命令窗口，在写代码的过程中，把部分代码粘到命令行去验证，事半功倍！