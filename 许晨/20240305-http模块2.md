```
import fs from 'fs';

// let arr = fs.readdirSync('./');

// console.log(arr);

function listArr(filePath) {
    let resultArr = [];
    // 读取所有的内容
    let arr = fs.readdirSync(filePath);

    // 将所有内容插入到结果数组

    resultArr = resultArr.concat(arr);

    // 遍历所有内容，检查是否有目录，如果有，则继续调用listArr函数，将函数执行的结果合并到结果数组中
    arr.forEach(item => {
        let lastStrIs = filePath.lastIndexOf('/') === filePath.length - 1;
        let tmpPath = lastStrIs ? filePath + item : `${filePath}/${item}`;
        let stat = fs.statSync(tmpPath);
        if (stat.isDirectory()) {
            let childArr = listArr(tmpPath);
            resultArr = resultArr.concat(childArr);
        }
    })

    return resultArr;
}

let arr = listArr('./');

console.log(arr);

```
